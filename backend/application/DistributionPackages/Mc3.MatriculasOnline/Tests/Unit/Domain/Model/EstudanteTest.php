<?php
namespace Mc3\MatriculasOnline\Tests\Unit\Domain\Model;

/*
 * This file is part of the Mc3.MatriculasOnline package.
 */

/**
 * Testcase for Estudante
 */
class EstudanteTest extends \Neos\Flow\Tests\UnitTestCase
{

    /**
     * @test
     */
    public function makeSureThatSomethingHolds()
    {
        $this->markTestIncomplete('Automatically generated test case; you need to adjust this!');

        $expected = 'Foo';
        $actual = 'Foo'; // This should be the result of some function call
        $this->assertSame($expected, $actual);
    }
}
