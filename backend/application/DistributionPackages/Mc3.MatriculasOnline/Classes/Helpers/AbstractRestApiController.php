<?php
namespace Mc3\MatriculasOnline\Helpers;

use Neos\Flow\Mvc\Controller\ActionController;

abstract class AbstractRestApiController extends ActionController{
    const HTTP_STATUS_ERROR = 500;
    const HTTP_STATUS_OK = 200;

    /**
     * A list of IANA media types which are supported by this controller
     *
     * @var array
     */
    protected $supportedMediaTypes = ['application/json'];
    
    public function actionError(){}
}