<?php

namespace Mc3\MatriculasOnline\Domain\Dto;


/**
 * Teste de classe usando EmailDto
 */
class EmailDto
{
	/**
	 * description
	 *
	 * @var string
	 */
	protected $email;
	
	/**
	 * @param string $email
	 */
	public function __construct($email)
	{
		$this->email = $email;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setEmail(Email $email)
	{
		$this->email = $email;
		return $this;
	}
}
