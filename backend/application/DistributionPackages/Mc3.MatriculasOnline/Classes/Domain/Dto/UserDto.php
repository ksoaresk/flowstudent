<?php
namespace Mc3\MatriculasOnline\Domain\Dto;

use Doctrine\Common\Collections\ArrayCollection;

class UserDto
{
	/**
	 * @var string
	 */
	private $email;
	
	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var array<EmailDto>
	 */
	private $listEmails;

	
	/**
	 * @param string $name
	 * @param string $email
	 * @param ArrayCollection<EmailDto> $listEmails
	 *
	 */
	public function __construct($name, $email, $listEmails)
	{
		$this->name       = $name;
		$this->email      = $email;
		$this->listEmails = $listEmails;
	}

	/**
	 * Gets the value of listEmails
	 *
	 * @return array<EmailDto>
	 */
	public function getListEmails()
	{
		return $this->listEmails;
	}

	/**
	 * Sets the value of listEmails
	 *
	 * @param array $listEmails description
	 *
	 * @return array
	 */
	public function setListEmails($listEmails)
	{
		$this->listEmails = $listEmails;
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}


	public function getEmail()
	{
		return $this->email;
	}

	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}
}

