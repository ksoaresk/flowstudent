<?php
namespace Mc3\MatriculasOnline\Domain\Repository;

/*
 * This file is part of the Mc3.MatriculasOnline package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class UserRepository extends Repository
{

    // add customized methods here
}
