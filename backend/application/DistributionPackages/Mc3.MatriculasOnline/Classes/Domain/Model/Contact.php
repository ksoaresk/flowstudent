<?php
namespace Mc3\MatriculasOnline\Domain\Model;

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 * @ORM\Table(name="contacts")
 */
class Contact
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var User
	 * @ORM\ManyToOne
     */
    protected $user_id;

    /**
     * @var string
     */
    protected $phone;


    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param string $user_id
     * @return void
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }
    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

	/**
	 * @param $phone
	 */
	public function __construct($phone)
	{
        $this->phone = $phone;
	}

	/**
	 * @param User $user
	 * @return void
	 */
	public function setUser(User $user)
	{
		$this->user_id = $user;
	}
}
