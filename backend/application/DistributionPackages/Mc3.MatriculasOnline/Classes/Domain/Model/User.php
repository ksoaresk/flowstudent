<?php
namespace Mc3\MatriculasOnline\Domain\Model;


use Neos\Flow\Annotations as Flow; 
use Doctrine\ORM\Mapping as ORM;
/**
 * @Flow\Entity
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=64)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, unique=true)
     * @Flow\Validate(type="EmailAddress")
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", options={"default":"1"})
     */
    protected $status=1;

    /**
     * @var \Datetime
     * @ORM\Column(type="datetime", options={"default" : "CURRENT_TIMESTAMP"})
     */
    protected $created_at;

    /**
     * @var \Datetime
     * @ORM\Column(type="datetime", options={"default" : "CURRENT_TIMESTAMP"})
     */
    protected $updated_at;

    /***
     * @var ArrayCollection<Contact>
     * @ORM\ManyToOne
     */
    protected $contacts;


    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name=$name;
    }
    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return void
     */
    public function setPassword($password)
    {
        $this->password=$password;
    }
    /**
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param boolean $status
     * @return void
     */
    public function setStatus($status)
    {
        $this-> status = $status;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail(){ 
		return $this->email;
	}

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAndUpdateAtValue()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }
    
    /**
     * @ORM\PreUpdate
     */
    public function setUpdateAt()
    {
        $this->updated_at = new \DateTime();
    }

	/**
	 * @param string $name
     * @param string $email
     * @param string $password
	 */
	public function __construct($name, $email, $password)
	{
        $this->name     = $name;
        $this->email    = $email;
        $this->password = $password;
		$this->contacts = new ArrayCollection();
	}
	
	/**
	 * undocumented function
	 * @param Contact $contact
	 * @return void
	 */
	public function addContact(Contact $contact)
	{
		$contact->setUser($this);
		$this->contacts->add($contact);
	}
	
}
