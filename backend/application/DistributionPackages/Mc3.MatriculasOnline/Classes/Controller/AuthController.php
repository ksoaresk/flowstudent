<?php

namespace Mc3\MatriculasOnline\Controller;

use Mc3\MatriculasOnline\Helpers\AbstractRestApiController;
use Mc3\MatriculasOnline\Domain\Dto\UserDto;

use Mc3\MatriculasOnline\Domain\Model\User;
use Neos\Flow\Annotations as Flow;

/**
 * undocumented class
 *
 * @package default
 * @subpackage default
 * @author Me
 * @Flow\Scope("singleton")
 */
class AuthController extends AbstractRestApiController 
{
    const SALTS_OF_CRYPT = 10;
    /**
    * @Flow\Inject
    * @var \Mc3\MatriculasOnline\Domain\Repository\UserRepository
    */
    protected $userRepository;

    public function indexAction()
    {
         // list($token) = $this->request->getHttpRequest()->getHeader('Authorization'); 
        
         $stdClass = new \stdClass;
         $stdClass->message = "Welcome to API 1.0 using Neo Flow PHP";
         
         $data = new \stdClass;
         $data->data = $stdClass;        
         $this->response->setStatusCode(AbstractRestApiController::HTTP_STATUS_OK);
      
         $this->response->setContentType('application/json');
        return json_encode($data);
    }

	/**
	 * @param UserDto $userDto
	 *
	 */
    public function loginAction($userDto)
    {
		// var_dump(( $userDto->getListEmails()->toArray() )[0]->getEmail());
		return $userDto->getListEmails();
    }

	protected function initializeLoginAction() {
		  $arg = $this->arguments->getArgument('userDto')
					  ->getPropertyMappingConfiguration();

			$arg->forProperty('listEmails')
			  ->setTypeConverterOption(
							\Neos\Flow\Property\TypeConverter\PersistentObjectConverter::class,
						  	\Neos\Flow\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
						  	true
						  );

		  $arg->allowAllProperties();
		  $arg ->setTypeConverterOption(
							  \Neos\Flow\Property\TypeConverter\PersistentObjectConverter::class,
							  \Neos\Flow\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
						      true
						  );
	}

    // /**
    // * @param Mc3\MatriculasOnline\Domain\Model\User $user
    // // * @Flow\MapRequestBody("$user")
    // * @return string
    // */
    /**
     * @var \Neos\Flow\Security\Cryptography\BCryptHashingStrategy;
     */
    protected $hash;
    public function registerAction()
    {
        $hash = new \Neos\Flow\Security\Cryptography\BCryptHashingStrategy(self::SALTS_OF_CRYPT);

        try {
            $password = $hash->hashPassword($this->request->getArgument('password'));
            $user = new User();
            $user->name = $this->request->getArgument('name');
            $user->email= $this->request->getArgument('email');
            $user->password = $password;

            $this->userRepository->add($user);

            $token = '1232456789';

            return json_encode([ 'data' => $user, 'token' => $token]);

        } 
        catch (\Exception $e) {
            return json_encode(['error' => true]);        
        }
        

    }
}
