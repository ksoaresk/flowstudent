<?php
namespace Mc3\MatriculasOnline\Controller;

/*
 * This file is part of the Mc3.MatriculasOnline package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;

class UserController extends ActionController
{

    /**
     * @Flow\Inject
     * @var \Mc3\MatriculasOnline\Domain\Repository\UserRepository
     */
    protected $userRepository;

    /**
     * @return void
     */
    public function indexAction()
    {
        $data = new \stdClass;

       $users = $this->userRepository->findAll()->toArray();
       
       $data->data = $users;
       return json_encode($data);
    }
}
