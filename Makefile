up:
	@docker-compose up -d

down: 
	@docker-compose down

restart:
	@docker-compose restart	

rebuild:
	@make down
	@docker-compose build
	@docker-compose up -d

logs:
	@docker-compose logs -f

run:
	@docker-compose exec backend ./flow $(filter-out $@, $(MAKECMDGOALS))

clean:
	@docker-compose exec backend ./flow flow:cache:flush 
